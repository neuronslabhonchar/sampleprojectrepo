from sklearn.linear_model import LinearRegression
from joblib import dump, load
import botocore

class LinearModel:
    '''
        Example of a model class that also can be custom
    '''
    def __init__(self, type = 'linear'):
        self.type = type
        self.model = LinearRegression()
        self.filename = None

    def fit(self, X, Y):
        self.model.fit(X, Y)

    def predict(self, X):
        return self.model.predict(X)

    def save_model(self, s3, bucket, filename, filename_local):
        dump(self.model, filename_local)
        s3.Bucket(bucket).put_object(Key=filename, Body=open(filename_local, 'rb'))

    def load_model_local(self, filename = None):
        if filename != None:
            self.filename = filename
        self.model = load(filename)

    def load_model_s3(self, s3, bucket, key, filename):
        try:
            s3.Bucket(bucket).download_file(key, filename)
        except Exception as e:
            print("The object does not exist.")
        self.filename = filename
        self.model = load(filename)