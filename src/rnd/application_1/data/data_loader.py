import numpy as np
import botocore

class RawDataLoader:
    '''
        Downloading data from the S3 bucket
    '''
    def __init__(self, s3):
        # initializing resource
        self.s3 = s3

    def load_data(self, bucket, key, save_name):
        try:
            self.s3.Bucket(bucket).download_file(key, save_name)
        except Exception as e:
            print("The object does not exist.")