from sklearn.preprocessing import StandardScaler

class DataPreprocessor:
    '''
        Example of data preprocessing class that can be custom
    '''
    def __init__(self, processing_type = 'normal'):
        self.processing_type = processing_type
        self.processor = StandardScaler()

    def fit(self, X):
        self.processor.fit(X)

    def fit_transform(self, X):
        return self.processor.fit_transform(X)

    def transform(self, X):
        return self.processor.transform(X)