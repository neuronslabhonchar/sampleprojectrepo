import boto3
import yaml
import os

import pandas as pd
from sklearn.model_selection import train_test_split

from ..data.data_loader import RawDataLoader
from ..data.data_processor import DataPreprocessor
from ..models.linear_model import LinearModel


ROOT_PATH = './'
with open(ROOT_PATH + 'config.yml', "r") as f:
    CONFIG = yaml.safe_load(f)

EXPERIMENT_VERSION = CONFIG['EXPERIMENT']['VERSION']
BUCKET = CONFIG['S3']['BUCKET']
DATASET_FOLDER = CONFIG['S3']['DATASET']
WEIGHTS_FOLDER = CONFIG['S3']['WEIGHTS']
FIGURES_FOLDER = CONFIG['S3']['FIGURES']

s3 = boto3.resource('s3')

FILE_NAME = 'dataset.csv'
FILE_PATH = os.path.join(DATASET_FOLDER, EXPERIMENT_VERSION, FILE_NAME)
FILE_PATH_LOCAL = os.path.join(ROOT_PATH, DATASET_FOLDER, FILE_NAME)

data_loader = RawDataLoader(s3)
data_loader.load_data(
    BUCKET, FILE_PATH, FILE_PATH_LOCAL
)

data = pd.read_csv(FILE_PATH_LOCAL).drop(columns = ['Unnamed: 0'])
X = data[['F1', 'F2', 'F3']]
Y = data['Y']

data_processor = DataPreprocessor()
X_processed = data_processor.fit_transform(X)

linear_model = LinearModel()
X_train, X_test, Y_train, Y_test = train_test_split(X_processed, Y, random_state=777)

linear_model.fit(X_train, Y_train)
linear_model.save_model(
                            s3,
                            BUCKET,
                            os.path.join(WEIGHTS_FOLDER, EXPERIMENT_VERSION, 'test.joblib'),
                            os.path.join(ROOT_PATH, WEIGHTS_FOLDER, 'test.joblib')
                        )
